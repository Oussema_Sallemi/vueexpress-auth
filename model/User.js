const mongoose = require("mongoose");
const emailValidator = require("email-validator");
const bcrypt = require("bcryptjs");

const SALT_ROUNDS = 6;
const UserSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      min: 6,
      max: 255,
      trim: true,
      lowercase: true,
      index: { unique: true },
      validate: {
        validator: emailValidator.validate,
        message: (props) => `${props.value} is not a valid email address!`,
      },
    },
    password: {
      type: String,
      required: true,
      max: 1024,
      min: 6,
      trim: true,
      index: { unique: true },
    },
    page: {
      type: String,
      required: true,
      max: 1024,
      min: 6,
      trim: true,
      index: { unique: true },
    },
    page_token: {
      type: String,
      required: true,
      max: 1024,
      min: 6,
      trim: true,
      index: { unique: true },
    },
  },
  { timestamps: true }
);

UserSchema.pre("save", async function preSave(next) {
  const user = this;
  if (!user.isModified("password")) return next();
  try {
    const hashedPassword = await bcrypt.hash(user.password, SALT_ROUNDS);
    user.password = hashedPassword;
    const hashedToken = await bcrypt.hash(user.page_token, SALT_ROUNDS);
    user.page_token = hashedToken;
    return next();
  } catch (err) {
    return next(err);
  }
});

UserSchema.methods.comparePassword = async function comparePassword(candidate) {
  return bcrypt.compare(candidate, this.password);
};
UserSchema.methods.comparePageToken = async function comparePageToken(
  candidate
) {
  return bcrypt.compare(candidate, this.page_token);
};

module.exports = mongoose.model("User", UserSchema);
