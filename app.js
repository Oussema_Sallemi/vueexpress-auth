const express = require("express");
const app = express();
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const MongoStore = require("connect-mongo")(session);
const { v4: uuidv4 } = require("uuid");

app.use(cors({ credentials: true, origin: "http://localhost:1234" }));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:1234");
  res.header("Access-Control-Allow-Credentials", true);
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
app.use(bodyParser.json({ extended: false }));
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(cookieParser());
dotenv.config();

//connect to database

mongoose.connect(
  process.env.DB_CONNECT,
  { useNewUrlParser: true, useUnifiedTopology: true },
  () => console.log("connected to database")
);

//import routes from auth.js

const userRoute = require("./routes/user");

app.use(
  session({
    name: "cma_session",
    genid: function(req) {
      console.log("session id created");
      return uuidv4();
    },
    secret: process.env.SESSION_SECRET,
    resave: true,
    saveUninitialized: false,
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    saveUninitialized: false,
    cookie: { secure: false, expires: 60000 },
  })
);
//Route middleweares to direct routes data

app.use("/users", userRoute);

//creating routes

app.get("/", (req, res) => {
  let today = new Date();
  res.send(`hello from auth service! ${today}`);
});

//start listening to the server
const port = 4321;
app.listen(port, () => console.log(`Auth Service started on port ${port}`));
