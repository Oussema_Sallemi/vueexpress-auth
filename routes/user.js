const express = require("express");
const UserModel = require("../model/User");
const ClientModel = require("../model/Client");
const CompanyModel = require("../model/Company");
const router = express.Router();
const jwt = require("jsonwebtoken");
const verifyToken = require("../middleware/verifyToken");

// @route POST /login
// @desc  Post user creds for login
router.post("/login", async (req, res) => {
  //check if user email exists in database

  let user = await UserModel.findOne({ email: req.body.email });
  if (!user) return res.status(400).send("Email id not found");

  //password is correct

  let validPass = await user.comparePassword(req.body.password);
  if (!validPass) return res.status(400).send("Invalid Password");

  //assign user._id to session

  req.session.user = user._id;
  req.session.isLoggedIn = true;

  //create and assign token

  let token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
  let sessionID = req.sessionID;
  // set JWT as cookie
  res.cookie("auth_token", token, { httpOnly: true });
  res.json({ token, sessionID });
});

// @route GET /logout
// @desc  logout user
router.get("/logout", (req, res) => {
  //destroy session here before logout
  req.session.destroy();
  return res.send("You are logged out!");
});

// @route POST /register
// @desc  POST user creds for regestration
router.post("/register", async (req, res, next) => {
  //check if user email in database

  let emailExist = await UserModel.findOne({ email: req.body.email });
  if (emailExist)
    return res
      .status(400)
      .send("Email already exists! Try with a different email please.");
  console.log(req.body);

  // create a new user
  const user = new UserModel({
    email: req.body.email,
    password: req.body.password,
    page: req.body.page,
    page_token: req.body.page_token,
  });

  //save user to database
  try {
    const savedUser = await user;
    savedUser.save();
    //create and assign token
    let token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
    // set JWT as cookie
    res.cookie("auth_token", token, { httpOnly: true });
    //assign user._id to session

    req.session.user = user._id;
    req.session.page = user.page;
    req.session.page_token = user.page_token;
    req.session.isLoggedIn = true;

    res.send({ user: user._id, token: token });
  } catch (err) {
    return next(err);
  }
});

// @route GET /account
// @desc  GET user creds from DB
router.post("/account", verifyToken, async (req, res) => {
  //check if user email exists in database

  let user = await UserModel.findOne({ email: req.body.email });
  try {
    if (!user) return res.status(400).send("Email id not found");
    if (user) return res.send({ user });
  } catch (err) {
    return next(err);
  }
});

// @route GET /protected
// @desc  GET specific data from DB for protected route
router.get("/protected", verifyToken, async (req, res) => {
  res.send({ clear: true });
});

// @route GET /all
// @desc  GET all users
router.get("/all", async (req, res) => {
  await ClientModel.find({}, function(err, users) {
    res.send(users);
  });
});

// @route POST /clients
// @desc  POST client creds for regestration
router.post("/clients", verifyToken, async (req, res, next) => {
  //check if user email in database

  let nameExist = await ClientModel.findOne({ name: req.body.name });
  if (nameExist)
    return res
      .status(400)
      .send("Client already exists! Try for a different client please.");

  // create a new client
  const client = new ClientModel({
    name: req.body.name,
    url: req.body.url,
  });

  //save client to database
  try {
    const savedClient = await client;
    savedClient.save();
    res.send({ client: client._id });
  } catch (err) {
    return next(err);
  }
});

// @route POST /comapnies
// @desc  POST company creds for regestration
router.post("/companies", verifyToken, async (req, res, next) => {
  //check if user email in database

  let nameExist = await CompanyModel.findOne({ name: req.body.name });
  if (nameExist)
    return res
      .status(400)
      .send("Company already exists! Try for a different company please.");

  // create a new company
  const company = new CompanyModel({
    name: req.body.name,
    url: req.body.url,
  });

  //save company to database
  try {
    const savedCompany = await company;
    savedCompany.save();
    res.send({ company: company._id });
  } catch (err) {
    return next(err);
  }
});

module.exports = router;
