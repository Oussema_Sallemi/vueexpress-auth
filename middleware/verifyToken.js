const dotenv = require("dotenv");
const jwt = require("jsonwebtoken");

dotenv.config();
const verifyToken = async (req, res, next) => {
  const token = req.headers.authorization || "";
  try {
    if (!token) {
      return res.status(401).json("You need to Login!");
    }
    const decrypt = await jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = {
      _id: decrypt._id,
    };
    next();
  } catch (err) {
    return res.status(500).json(err.toString());
  }
};

module.exports = verifyToken;
